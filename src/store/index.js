import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    // user : ''
    division:''
  },
  mutations: {
    auth_request(state){
      state.status = 'loading'
    },
    auth_success(state, token){
      state.status = 'success'
      state.token = token
      state.division = localStorage.division
      // state.user = user
    },
    auth_error(state){
      state.status = 'error'
    },
    logout(state){
      state.status = ''
      state.token = ''
    },
  },
  actions: {
    login({commit}, user){
      return new Promise((resolve, reject) => {
        commit('auth_request')
        axios({url: `${process.env.VUE_APP_BE}login`, data: user, method: 'POST' })
        .then(resp => {
          const token = resp.data.token
          const division = resp.data.division
          localStorage.setItem('token', token)
          localStorage.setItem('division', division)
          localStorage.setItem('name', resp.data.name)
          axios.defaults.headers.common['Authorization'] = token
          // console.log("resp")
          // console.log(user)
          commit('auth_success', token)
          resolve(resp)
        })
        .catch(err => {
          commit('auth_error')
          localStorage.removeItem('token')
          reject(err)
        })
      })
  },
  },
  getters : {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status
  }
})